<?php
require 'functions.php';

if (!isset($_SESSION['authentication'])) { ?>
  <script type="text/javascript">
    window.location = "login.php"
  </script>
  You're not ready logged in, redirecting you.
<?php } else {
  //ambil data dari function
  $data = query("datacovid19")->find();

?>

  <!DOCTYPE html>
  <html>

  <head>
    <title>Data Covid 19 | Kab. Bandung Barat</title>
    <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
  </head>

  <body>
    <div class="container">
      <br>
      <CENTER>
        <h1>Data Covid-19 Kab. Bandung Barat</h1>
      </CENTER>
      <a href="tambah.php" class="btn btn-success">Tambah Kecamatan</a>
      <a href="logout.php" class="btn btn-danger">LogOut</a>
      <?php
      if (isset($_SESSION['success'])) {
        echo "<div class='alert alert-success'>" . $_SESSION['success'] . "</div>";
      }
      ?>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Kecamatan</th>
            <th scope="col">Positif</th>
            <th scope="col">PDP</th>
            <th scope="col">ODP</th>
            <th scope="col">Sembuh</th>
            <th scope="col">Meninggal</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <?php
        foreach ($data as $row) :
          echo "<tr>";
          echo "<th scope='row'>" . $row->Kecamatan . "</th>";
          echo "<td>" . $row->Positif . "</td>";
          echo "<td>" . $row->PDP . "</td>";
          echo "<td>" . $row->ODP . "</td>";
          echo "<td>" . $row->Sembuh . "</td>";
          echo "<td>" . $row->Meninggal . "</td>";

          echo "<td>";
          echo "<a href = 'ubah.php?id=" . $row->_id . "'class='btn btn-primary'>EDIT</a>";
          echo "<a href = 'hapus.php?id=" . $row->_id . "'class='btn btn-danger'>HAPUS</a>";
          echo "</td>";
          echo "</tr>";
        endforeach;
        ?>
      </table>
    </div>
  </body>

  </html>
<?php } ?>