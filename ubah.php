<?php
require 'functions.php';


//ambil data dari function
if (isset($_GET['id'])) {
  $data = query("datacovid19")->findOne(['_id' => new MongoDB\BSON\ObjectID($_GET['id'])]);
}


//cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST['submit'])) {
  ubah("datacovid19", $_POST);
  header("Location: index.php");
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>Data Covid 19 | Kab. Bandung Barat</title>
  <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>

<body>
  <div class="container">
    <br>
    <CENTER>
      <h1>Edit Covid-19 Kab. bandung Barat</h1>
    </CENTER>
    <form method="POST">
      <div class="form-group">
        <input type="hidden" name="id" value="<?= $data['_id'];  ?>">
        <label for="Kecamatan"><strong>Kecamatan:</strong></label>
        <select name="Kecamatan" id="Kecamatan" class="form-control">
          <option value="">-- Kecamatan --</option>
          <option value="Batujajar">Batujajar</option>
          <option value="Cihampelas">Cihampelas</option>
          <option value="Cikalong Wetan">Cikalong Wetan</option>
          <option value="Cililin">Cililin</option>
          <option value="Cipatat">Cipatat</option>
          <option value="Ngamprah">Ngamprah</option>
          <option value="Lembang">Lembang</option>
          <option value="Padalarang">Padalarang</option>
          <option value="Cisarua">Cisarua</option>
          <option value="Parongpong">Parongpong</option>
        </select>
        <label for="Positif"><strong>Positif:</strong></label>
        <input type="text" class="form-control" name="Positif" required="" placeholder="Positif" value="<?= $data['Positif'];  ?>">
        <label for="PDP"><strong>PDP:</strong></label>
        <input type="text" class="form-control" name="PDP" placeholder="PDP" value="<?= $data['PDP'] ?>">
        <label for="ODP"><strong>ODP:</strong></label>
        <input type="text" class="form-control" name="ODP" placeholder="ODP" value="<?= $data['ODP'];  ?>">
        <label for="Sembuh"><strong>Sembuh:</strong></label>
        <input type="text" class="form-control" name="Sembuh" placeholder="Sembuh" value="<?= $data['Sembuh'];  ?>">
        <label for="Meninggal"><strong>Meninggal:</strong></label>
        <input type="text" class="form-control" name="Meninggal" placeholder="Meninggal" value="<?= $data['Meninggal'];  ?>">

        <br>
        <button type="submit" name="submit" class="btn btn-success">Ubah</button>
        <a href="index.php" class="btn btn-primary">Kembali</a>
      </div>
    </form>
  </div>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ubah data pegawai</title>
</head>