<?php
session_start();
require_once __DIR__ . "/vendor/autoload.php";
$conn = (new MongoDB\Client)->dbcovid19;
global $conn;
function login($data)
{
  global $conn;
  $user = $data['username'];
  $pass = $data['password'];
  $cek_collection = $conn->users;
  $userDatabaseFind = $cek_collection->find(array('Username' => $user, 'Password' => $pass));

  //Iterates through the found results
  foreach ($userDatabaseFind as $userFind) {
    $storedUsername = $userFind['Username'];
    $storedPassword = $userFind['Password'];
  }

  if ($user == $storedUsername && $pass == $storedPassword) {
    $_SESSION['authentication'] = true;

    header("location: index.php");
  } else {
    $_SESSION['failed'] = true;
    header("location: login.php");
  }
}

function query($query)
{
  global $conn;
  $result = $conn->$query;
  return $result;
}
function tambah($query, $data)
{
  global $conn;
  $Kecamatan = htmlspecialchars($data['Kecamatan']);
  $Positif = htmlspecialchars($data['Positif']);
  $PDP = htmlspecialchars($data['PDP']);
  $ODP = htmlspecialchars($data['ODP']);
  $Sembuh = htmlspecialchars($data['Sembuh']);
  $Meninggal = htmlspecialchars($data['Meninggal']);
  $result = $conn->$query->insertOne([
    'Kecamatan' => $Kecamatan,
    'Positif' => $Positif,
    'PDP' => $PDP,
    'ODP' => $ODP,
    'Sembuh' => $Sembuh,
    'Meninggal' => $Meninggal,
  ]);

  $status = $_SESSION['success'] = "Data Berhasil di tambahkan";
  return $status;
}
function hapus($query, $id)
{
  global $conn;
  $conn->$query->deleteOne(['_id' => new MongoDB\BSON\ObjectID($id['id'])]);
  $_SESSION['success'] = "Data Berhasil dihapus";
}

function ubah($query, $data)
{
  global $conn;
  $id = $data['id'];
  $Kecamatan = htmlspecialchars($data['Kecamatan']);
  $Positif = htmlspecialchars($data['Positif']);
  $PDP = htmlspecialchars($data['PDP']);
  $ODP = htmlspecialchars($data['ODP']);
  $Sembuh = htmlspecialchars($data['Sembuh']);
  $Meninggal = htmlspecialchars($data['Meninggal']);

  $conn->$query->updateOne(
    ['_id' => new MongoDB\BSON\ObjectID($id)],
    ['$set' => [
      'Kecamatan' => $Kecamatan,
      'Positif' => $Positif,
      'PDP' => $PDP,
      'ODP' => $ODP,
      'Sembuh' => $Sembuh,
      'Meninggal' => $Meninggal,
    ]]
  );
  $status = $_SESSION['success'] = "Data berhasil diubah";
  return $status;
}
