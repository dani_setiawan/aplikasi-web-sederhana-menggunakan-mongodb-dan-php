<?php
require 'functions.php';
if (isset($_GET['id'])) {
  $data = query("datacovid19")->findOne(['_id' => new MongoDB\BSON\ObjectID($_GET['id'])]);
}
if (isset($_POST['submit'])) {
  hapus("datacovid19", $_POST);
  header("Location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
  <title>APLIKASI INTERAKTIF</title>
  <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>

<body>
  <div class="container">
    <br>
    <CENTER>
      <h1>Hapus Data Covid-19 Kab. bandung Barat</h1>
    </CENTER>
    <h3> Di Kecamatan <?php echo "$data->Kecamatan"; ?> dengan Jumlah Positif <?php echo "$data->Positif"; ?> ? </h3>
    <form method="POST">
      <div class="form-group">
        <input type="hidden" value="<?php echo "$data->_id"; ?>" class="form-control" name="id">
        <a href="index.php" class="btn btn-primary">Kembali</a>
        <button type="submit" name="submit" class="btn btn-danger">Hapus</button>
      </div>
    </form>
  </div>
</body>

</html>