<?php
require 'functions.php';

//cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST['submit'])) {
  //ambil data dari tiap elemen pada form
  //cek datanya berhasil disimpan atau tidak
  tambah("datacovid19", $_POST);
  header("Location: index.php");
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>Data Covid 19 | Kab. Bandung Barat</title>
  <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>

<body>
  <div class="container">
    <br>
    <CENTER>
      <h1>Tambah Data Covid-19 Kab. bandung Barat</h1>
    </CENTER>
    <hr>
    <form method="POST">
      <div class="form-group">
        <label for="Kecamatan"><strong>Kecamatan:</strong></label>
        <select name="Kecamatan" id="Kecamatan" class="form-control">
          <option value="">-- Kecamatan --</option>
          <option value="Batujajar">Batujajar</option>
          <option value="Cihampelas">Cihampelas</option>
          <option value="Cikalong Wetan">Cikalong Wetan</option>
          <option value="Cililin">Cililin</option>
          <option value="Cipatat">Cipatat</option>
          <option value="Ngamprah">Ngamprah</option>
          <option value="Lembang">Lembang</option>
          <option value="Padalarang">Padalarang</option>
          <option value="Cisarua">Cisarua</option>
          <option value="Parongpong">Parongpong</option>
        </select>
        <label for="Positif"><strong>Positif:</strong></label>
        <input type="text" class="form-control" name="Positif" required="" placeholder="1-99">
        <label for="PDP"><strong>PDP:</strong></label>
        <input type="text" class="form-control" name="PDP" placeholder="1-99">
        <label for="ODP"><strong>ODP:</strong></label>
        <input type="text" class="form-control" name="ODP" placeholder="1-99">
        <label for="Sembuh"><strong>Sembuh:</strong></label>
        <input type="text" class="form-control" name="Sembuh" placeholder="1-99">
        <label for="Meninggal"><strong>Meninggal:</strong></label>
        <input type="text" class="form-control" name="Meninggal" placeholder="1-99">

        <br>
        <button type="submit" name="submit" class="btn btn-success">Tambah</button>
        <a href="index.php" class="btn btn-primary">Kembali</a>
      </div>
    </form>
  </div>
</body>

</html>


<!-- <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tambah data pegawai</title>
</head>

<body>
  <h1>Tambah data Pegawai!</h1> <br>
  <form action="" method="POST">
    <label for="nama">Nama : </label>
    <input type="text" name="nama" id="nama" required> <br>

    <label for="umur">Umur : </label>
    <input type="text" name="umur" id="umur" required><br>

    <label for="alamat">Alamat : </label>
    <textarea name="alamat" id="alamat" required></textarea> <br>

    <button type="submit" name="submit">Tambah Data</button>


  </form>
</body>

</html> -->